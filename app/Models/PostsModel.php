<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class PostsModel extends Model
{
    
	public $id;
	public $author_id;
	public $post_image;
	public $title;
	public $content;
	public $created_at;
	public $updated_at;

}
<?php


namespace App\Helpers;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class GoogleFCM
{

    private $androidRegId = [];
    private $iosRegId = [];
    private $serverKey = null;

    private $title;
    private $body;
    private $data = [];

    public function __construct($serverKey = null)
    {
        if(is_null($serverKey)) {
            $this->serverKey = config("default.GOOGLE_FCM_SERVER_KEY");
        } else {
            $this->serverKey = $serverKey;
        }
    }

    public function setTitle($text)
    {
        $this->title = trim(strip_tags($text));
    }

    public function setBody($text)
    {
        $this->body = $text;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Add reg id for android
     * @param array|string $regId
     */
    public function addAndroidRegId($regId)
    {
        if(is_array($regId)) {
            $this->androidRegId = array_merge($this->androidRegId, $regId);
        } else {
            $this->androidRegId[] = $regId;
        }
    }


    /**
     * Add reg id for ios
     * @param $regId
     */
    public function addIosRegId($regId)
    {
        if(is_array($regId)) {
            $this->iosRegId = array_merge($this->iosRegId, $regId);
        } else {
            $this->iosRegId[] = $regId;
        }
    }

    /**
     * @return void
     */
    public function send()
    {
        $params = [];
        $params['registration_ids'] = $this->androidRegId;
        $params['data']['title'] = trim(strip_tags($this->title));
        $params['data']['body'] = $this->body;
        $params['data'] = array_merge($params['data'], $this->data);
        $params['priority'] = 'high';
        $headers = [
            'Authorization:key='.$this->serverKey,
            'Content-Type:application/json',
        ];

        $client = new Client();
        try{
            if(count($this->androidRegId)) {
                $params['registration_ids'] = $this->androidRegId;
                $client->sendAsync((new Request("POST",'https://fcm.googleapis.com/fcm/send', $headers, json_encode($params))));
            }

            if(count($this->iosRegId)) {
                $params['registration_ids'] = $this->iosRegId;
                $params['content_available']= true;
                $params['notification'] = [
                  'sound'=> 'default',
                  'badge'=> 0,
                  'title'=> $this->title,
                  'body'=> $this->body
                ];
                $client->sendAsync((new Request("POST",'https://fcm.googleapis.com/fcm/send', $headers, json_encode($params))));
            }
        }
        catch (\Exception $e){
            app("log")->debug($e->getMessage());
        }
    }

}

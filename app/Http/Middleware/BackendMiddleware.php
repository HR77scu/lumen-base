<?php

namespace App\Http\Middleware;

use App\Helpers\Auth;
use App\Helpers\Redirect;
use Closure;

class BackendMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::isLoggedIn()) {
            return Redirect::backend("auth/login","Please login for first!","warning");
        }

        return $next($request);
    }
}

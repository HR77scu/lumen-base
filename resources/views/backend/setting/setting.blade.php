@extends("layout.backend.layout")
@section("content")
    <div class="page-header">
        <h4 class="page-title">Setting</h4>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="{{ backend_url() }}">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="{{ request()->url() }}">Setting</a>
            </li>
        </ul>
    </div>

    {!! alert_message() !!}

    <div class="row">
        <div class="col-md-8">
            <div class="card card-with-nav">
                <div class="card-header">
                    <div class="row row-nav-line">
                        <ul class="nav nav-tabs nav-line nav-color-secondary" role="tablist">
                            {{--                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#home" role="tab" aria-selected="true">Timeline</a> </li>--}}
                            <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#general" role="tab" aria-selected="false">General</a> </li>
                            {{--                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Settings</a> </li>--}}
                        </ul>
                    </div>
                </div>
                <form method="post" enctype="multipart/form-data" action="{{ backend_url("setting/save") }}">
                    {!! csrf_field() !!}
                    <div class="card-body">
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="form-group form-group-default">
                                    <label>Setting 1</label>
                                    <input type="text" required class="form-control" name="setting1" placeholder="Setting 1" value="{{ get_setting("setting1") }}">
                                </div>
                                <div class="form-group form-group-default">
                                    <label>Setting Upload 1</label>
                                    @if(get_setting("setting_upload"))
                                        <p><a target="_blank" href="{{ asset(get_setting("setting_upload")) }}"><img src="{{ asset(get_setting("setting_upload")) }}" width="100px" alt="Setting"></a></p>
                                    @endif
                                    <input type="file" accept=".jpg,.jpeg,.png" class="form-control" name="setting_upload">
                                    <div class="help-block">Support only jpg, jpeg and png</div>
                                </div>
                            </div>
                        </div>

                        <div class="text-right mt-3 mb-3">
                            <button class="btn btn-success">Save Setting</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>
@endsection

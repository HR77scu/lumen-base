<?php

namespace App\Http\Controllers\Backend;

use App\Exceptions\ValidateException;
use App\Helpers\Auth;
use App\Helpers\Hash;
use App\Helpers\Redirect;
use App\Helpers\Uploader;
use App\Helpers\Validate;
use App\Http\Controllers\Controller;
use App\Repositories\Settings;
use App\Repositories\Users;

class SettingController extends Controller
{

    public function __construct()
    {
        view()->share(['menu'=>""]);
    }

    public function getIndex()
    {
        $data = [];
        return view("backend.setting.setting", $data);
    }

    public function postSave()
    {
        $posts = request()->except(["_token"]);
        foreach($posts as $key => $val)
        {
            if(request()->hasFile($key)) {
                $val = Uploader::uploadFile($key);
            }

            $setting = Settings::findBy("name", $key);
            $setting->name = $key;
            $setting->content = $val;
            $setting->save();
            put_setting($key, $val);
        }

        return Redirect::back("The setting has been updated!","success");
    }
}

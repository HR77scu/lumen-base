<?php


namespace App\Helpers;


class Hash
{
    /**
     * Generate a hash
     * @param string $password
     * @return string
     */
    public static function make(string $password): string
    {
        return app("hash")->make($password);
    }

    /**
     * Compare a password with hashed password
     * @param string $password
     * @param string $hashedPassword
     * @return bool
     */
    public static function check(string $password, string $hashedPassword): bool
    {
        return app("hash")->check($password, $hashedPassword);
    }

}

<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class UsersModel extends Model
{
    
	public $id;
	public $created_at;
	public $updated_at;
	public $name;
	public $photo;
	public $email;
	public $password;
	public $last_login_at;
	public $last_login_browser;

}
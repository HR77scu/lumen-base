<?php

if(!function_exists('csrf_token'))
{
    /**
     * Generate a csrf token
     * @return string
     */
    function csrf_token(): string
    {
        return request()->session()->token();
    }
}

if(!function_exists('csrf_field'))
{
    /**
     * Generate html input csrf token
     * @return string
     */
    function csrf_field()
    {
        return "<input type='hidden' name='_token' value='".csrf_token()."'/>";
    }
}

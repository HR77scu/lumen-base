<?php

/** @var \Laravel\Lumen\Routing\Router $router */

// BACKEND Routes
$router->group(["prefix" => config("default.BACKEND_PATH")], function($route) {
    // AUTH Route
    $route->group(["prefix" => "auth"], function ($routeAuth) {
        $routeAuth->get("login", "Backend\AuthController@getLogin");
        $routeAuth->post("login", "Backend\AuthController@postLogin");
        $routeAuth->get("logout", "Backend\AuthController@getLogout");
        // Google Sign In
        $routeAuth->post("sign-google", "Backend\AuthController@postSignGoogle");
    });

    // Start Backend Middleware
    $route->group(["middleware"=>\App\Http\Middleware\BackendMiddleware::class], function() use ($route) {
        // Dashboard
        $route->get("/", "Backend\DashboardController@getIndex");

        // Profile
        $route->get("profile","Backend\ProfileController@getIndex");
        $route->post("profile/save","Backend\ProfileController@postSave");

        // Setting
        $route->get("setting","Backend\SettingController@getIndex");
        $route->post("setting/save","Backend\SettingController@postSave");

        // Books
        $route->get("books","Backend\BooksController@getIndex");

        // posts
        $route->get('post','Backend\PostController@getIndex');
        $route->get('post/create','Backend\PostController@getcreate');
        // routeController()
        $route->post('post/store','Backend\PostController@postCreate');

		}); // End Backend Middleware (Do not remove this line)


});

// API Routes
$router->group(["prefix"=>"api"], function($route) {
    $route->get("default", "API\ApiDefaultController@getIndex");
});

// Default Route
$router->get('/', "Frontend\HomeController@getIndex");






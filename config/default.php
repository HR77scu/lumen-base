<?php

return [
  "BACKEND_PATH"=> "admin",
  
  "GOOGLE_FCM_SERVER_KEY"=> env("GOOGLE_FCM_SERVER_KEY"),

  "DEFAULT_PROFILE_PHOTO"=> "assets/themes/azzara/img/profile.jpg"
];

<?php


namespace App\Helpers;


class Redirect
{
    public static function back(string $message, string $type = "info")
    {
        request()->session()->flash("alert_message", $message);
        request()->session()->flash("alert_type", $type);

        $referer = $_SERVER['HTTP_REFERER'];
        if(parse_url($referer, PHP_URL_HOST) == $_SERVER['SERVER_NAME'] && filter_var($referer, FILTER_VALIDATE_URL)) {
            return redirect($referer);
        } else {
            return redirect("/");
        }
    }

    /**
     * Redirect to a path
     * @param string $path
     * @param null $message
     * @param string $type
     * @return \Illuminate\Http\RedirectResponse|\Laravel\Lumen\Http\Redirector
     */
    public static function to(string $path, $message = null, $type = "info")
    {
        if(!is_null($message) && !is_null($type)) {
            request()->session()->flash("alert_message", $message);
            request()->session()->flash("alert_type", $type);
        }

        return redirect($path);
    }


    /**
     * Redirect to backend
     * @param string $path
     * @param null $message
     * @param string $type
     * @return \Illuminate\Http\RedirectResponse|\Laravel\Lumen\Http\Redirector
     */
    public static function backend(string $path, $message = null, $type = "info")
    {
        return static::to(backend_url($path), $message, $type);
    }
}

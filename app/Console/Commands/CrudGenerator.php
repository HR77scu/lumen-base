<?php


namespace App\Console\Commands;


use Crocodic\LaravelModel\Core\Model;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CrudGenerator extends Command
{
    protected $signature = "make:crud {table} {crud_name}";

    protected $description = "Generate crud from a table";

    public function handle(): void
    {
        $table = $this->argument("table");
        $crudName = $this->argument("crud_name");
        if(is_string($table) && is_string($crudName)) {
            // Define parameters
            $tableModel = "\App\Models\\".$this->convertTableToModelName($table);
            $tableModelInit = new $tableModel;
            $columns = $this->extractColumns($tableModelInit);
            $crudSlug = Str::slug($crudName);
            $controllerClassName = str_replace(" ","",ucwords(Str::slug($crudName," ")))."Controller";

            // Create a Controller
            $this->createController($columns, $crudSlug, $crudName, $controllerClassName);

            // Create routing
            $this->createRoute($crudSlug, $controllerClassName);

            // Create a browse view
            $this->createBrowseView($columns, $crudSlug, $crudName);

            // Create a create view
            $this->createFormView($columns, $crudSlug, $crudName);

            // Create a detail view
            $this->createDetailView($columns, $crudSlug, $crudName);

            // Add menu to sidebar
            $this->addSidebarMenu($crudSlug, $crudName);

        } else {
            $this->error("Please input table and crud name");
        }
    }

    private function createController(array $columns, string $crudSlug, string $crudName, string $controllerClassName): void
    {
        $controllerTemplate = file_get_contents(base_path("app/Console/Commands/Stubs/crud_generator/ExampleController.php.stub"));
        $directoryName = str_replace("-","_",$crudSlug);
        $crudRepo = str_replace(" ","",ucwords(Str::slug($crudName," ")));

        $modelAssign = "";
        foreach($columns as $column)
        {
            if(!in_array($column,['created_at','updated_at','deleted_at','id'])) {
                $modelAssign .= "\t\t\t".'$data->'.$column.' = request("'.$column.'");'."\n";
            }
        }
        $modelAssign = trim($modelAssign);

        $validateRule = [];
        foreach($columns as $column)
        {
            if(!in_array($column,['created_at','updated_at','deleted_at','id'])) {
                $validateRule[] = "\t\t\t\t".'"'.$column.'"=>"required"';
            }
        }
        $validateRule = trim(implode(",\n", $validateRule));

        $searchCondition = "";
        foreach($columns as $column)
        {
            if(!in_array($column,['created_at','updated_at','deleted_at','id'])) {
                $searchCondition .= "\t\t\t\t\t".'$sub->orWhere("'.$column.'","like","%".request("search")."%");'."\n";
            }
        }
        $searchCondition = trim($searchCondition);

        $controllerTemplate  = str_replace([
            "{crud_name}",
            "{crud_slug}",
            "{crud_repo}",
            "{crud_directory}",
            "{model_assign}",
            "{controller_class_name}",
            "{validate_rule_create}",
            "{validate_rule_update}",
            "{search_condition}"
        ],[
            $crudName,
            $crudSlug,
            $crudRepo,
            $directoryName,
            $modelAssign,
            $controllerClassName,
            $validateRule,
            $validateRule,
            $searchCondition
        ], $controllerTemplate);

        // Create view file
        file_put_contents(base_path("app/Http/Controllers/Backend/".$controllerClassName.".php"),$controllerTemplate);

        $this->info("Create controller succeed!");
    }

    private function createRoute(string $crudSlug, string $controllerClassName): void
    {
        $routeTemplate = file_get_contents(base_path("routes/web.php"));

        $newRoute = "\n\n\t\t"."//".$crudSlug."\n";
        $newRoute .= "\t\t".'$route->get("'.$crudSlug.'","Backend\\'.$controllerClassName.'@getIndex");'."\n";
        $newRoute .= "\t\t".'$route->get("'.$crudSlug.'/create","Backend\\'.$controllerClassName.'@getCreate");'."\n";
        $newRoute .= "\t\t".'$route->post("'.$crudSlug.'/create","Backend\\'.$controllerClassName.'@postCreate");'."\n";
        $newRoute .= "\t\t".'$route->get("'.$crudSlug.'/read/{id}","Backend\\'.$controllerClassName.'@getRead");'."\n";
        $newRoute .= "\t\t".'$route->get("'.$crudSlug.'/update/{id}","Backend\\'.$controllerClassName.'@getUpdate");'."\n";
        $newRoute .= "\t\t".'$route->post("'.$crudSlug.'/update/{id}","Backend\\'.$controllerClassName.'@postUpdate");'."\n";
        $newRoute .= "\t\t".'$route->get("'.$crudSlug.'/delete/{baseId}","Backend\\'.$controllerClassName.'@getDelete");'."\n";
        $newRoute .= "\t\t}); // End Backend Middleware (Do not remove this line)";

        if(stripos($routeTemplate,"}); // End Backend Middleware (Do not remove this line)") !== false) {
            $routeTemplate = str_replace("}); // End Backend Middleware (Do not remove this line)",$newRoute, $routeTemplate);

            file_put_contents(base_path("routes/web.php"), $routeTemplate);

            $this->info("Create route succeed!");
        } else {
            $this->warn("Create route failed, please create it manually!");
        }
    }

    private function createBrowseView(array $columns, string $crudSlug, string $crudName): void
    {
        // Assigning template
        $thColumnsTemplate = file_get_contents(base_path("app/Console/Commands/Stubs/crud_generator/th_columns.blade.php.stub"));
        $thColumns = "";
        foreach($columns as $column)
        {
            if(!in_array($column,["id","deleted_at","updated_at"])) {
                $label = $this->convertKebabCaseToHuman($column);
                $thColumns .= "\t\t\t\t\t".str_replace("{column}",$label, $thColumnsTemplate);
            }
        }
        $thColumns = trim($thColumns);

        $tdColumnsTemplate = file_get_contents(base_path("app/Console/Commands/Stubs/crud_generator/td_columns.blade.php.stub"));
        $tdColumns = "";
        foreach($columns as $column)
        {
            if(!in_array($column,["id","deleted_at","updated_at"])) {
                $tdColumns .= "\t\t\t\t\t".str_replace("{column}", '{{$item->' . $column . '}}', $tdColumnsTemplate);
            }
        }
        $tdColumns = trim($tdColumns);

        $browseTemplate = file_get_contents(base_path("app/Console/Commands/Stubs/crud_generator/browse.blade.php.stub"));
        $browseTemplate = str_replace([
            "{crud_name}",
            "{crud_slug}",
            "{th_columns}",
            "{td_columns}",
            "{column_count}"
        ],[
            $crudName,
            $crudSlug,
            $thColumns,
            $tdColumns,
            count($columns)
        ],$browseTemplate);

        // Put to destination
        // Make sure the directory exists
        $directoryName = str_replace("-","_",$crudSlug);
        if(!file_exists(base_path("resources/views/backend/".$directoryName))) {
            mkdir(base_path("resources/views/backend/".$directoryName), 0755);
        }
        // Create view file
        file_put_contents(base_path("resources/views/backend/".$directoryName."/browse.blade.php"),$browseTemplate);

        $this->info("Create browse view succeed!");
    }

    private function createFormView(array $columns, string $crudSlug, string $crudName): void
    {
        // Assigning template
        $formGroupTemplate = file_get_contents(base_path("app/Console/Commands/Stubs/crud_generator/form_group.blade.php.stub"));
        $formGroup = "";
        foreach($columns as $column)
        {
            if(!in_array($column,["id","created_at","deleted_at","updated_at"])) {
                $inputLabel = $this->convertKebabCaseToHuman($column);
                $inputName = $column;
                $inputValue = '{{ isset($row) ? $row->'.$inputName.' : null }}';
                $formGroup .= str_replace([
                        "{input_label}",
                        "{input_name}",
                        "{input_value}"
                    ],[
                        $inputLabel,
                        $inputName,
                        $inputValue
                    ],$formGroupTemplate)."\n";
            }
        }
        $formGroup = trim($formGroup);

        $formTemplate = file_get_contents(base_path("app/Console/Commands/Stubs/crud_generator/form.blade.php.stub"));
        $formTemplate = str_replace([
            "{crud_name}",
            "{crud_slug}",
            "{form_group}"
        ],[
            $crudName,
            $crudSlug,
            $formGroup
        ],$formTemplate);

        // Put to destination
        // Make sure the directory exists
        $directoryName = str_replace("-","_",$crudSlug);
        if(!file_exists(base_path("resources/views/backend/".$directoryName))) {
            mkdir(base_path("resources/views/backend/".$directoryName), 0755);
        }
        // Create view file
        file_put_contents(base_path("resources/views/backend/".$directoryName."/form.blade.php"),$formTemplate);

        $this->info("Create form succeed!");
    }

    private function createDetailView(array $columns, string $crudSlug, string $crudName): void
    {
        // Assigning template
        $formGroupTemplate = file_get_contents(base_path("app/Console/Commands/Stubs/crud_generator/form_group_detail.blade.php.stub"));
        $formGroup = "";
        foreach($columns as $column)
        {
            if(!in_array($column,["created_at","deleted_at","updated_at"])) {
                $inputLabel = $this->convertKebabCaseToHuman($column);
                $inputName = $column;
                $inputValue = '{{ $row->'.$inputName.' }}';
                $formGroup .= str_replace([
                        "{input_label}",
                        "{input_name}",
                        "{input_value}"
                    ],[
                        $inputLabel,
                        $inputName,
                        $inputValue
                    ],$formGroupTemplate)."\n";
            }
        }
        $formGroup = trim($formGroup);

        $detailTemplate = file_get_contents(base_path("app/Console/Commands/Stubs/crud_generator/detail.blade.php.stub"));
        $detailTemplate = str_replace([
            "{crud_name}",
            "{crud_slug}",
            "{form_group}"
        ],[
            $crudName,
            $crudSlug,
            $formGroup
        ],$detailTemplate);

        // Put to destination
        // Make sure the directory exists
        $directoryName = str_replace("-","_",$crudSlug);
        if(!file_exists(base_path("resources/views/backend/".$directoryName))) {
            mkdir(base_path("resources/views/backend/".$directoryName), 0755);
        }
        // Create view file
        file_put_contents(base_path("resources/views/backend/".$directoryName."/detail.blade.php"),$detailTemplate);

        $this->info("Create detail succeed!");
    }

    private function addSidebarMenu(string $crudSlug, string $crudName): void
    {
        $sidebarTemplate = file_get_contents(base_path("resources/views/layout/backend/sidebar.blade.php"));

        $sidebarMenuTemplate = file_get_contents(base_path("app/Console/Commands/Stubs/crud_generator/sidebar_menu.blade.php.stub"));
        $sidebarMenuTemplate = "\t\t".str_replace([
                "{crud_slug}",
                "{crud_name}"
            ],[
                $crudSlug,
                $crudName
            ], $sidebarMenuTemplate);
        $sidebarMenuTemplate .= "\n<!-- Additional Menu Here (Don't remove this line) -->";
        $sidebarMenuTemplate = trim($sidebarMenuTemplate);

        $sidebarTemplate = str_replace("<!-- Additional Menu Here (Don't remove this line) -->",$sidebarMenuTemplate, $sidebarTemplate);

        file_put_contents(base_path("resources/views/layout/backend/sidebar.blade.php"), $sidebarTemplate);

        $this->info("Sidebar menu added!");
    }

    private function convertTableToModelName(string $table): string
    {
        return str_replace(" ","",ucwords(str_replace("_"," ", $table)))."Model";
    }

    private function convertKebabCaseToHuman(string $text): string
    {
        return ucwords(str_replace("_"," ", $text));
    }

    private function extractColumns(Model $model)
    {
        $vars = get_object_vars($model);
        $columns = [];
        foreach($vars as $key => $val)
        {
            $columns[] = $key;
        }
        return $columns;
    }

}

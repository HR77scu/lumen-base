<?php


namespace App\Http\Controllers\Backend;

use App\Exceptions\ValidateException;
use App\Helpers\Auth;
use App\Helpers\Redirect;
use App\Helpers\Validate;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ThirdParty\GoogleSignIn\GoogleSignIn;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use GoogleSignIn;

    public function __construct()
    {
        $this->middleware(VerifyCsrfToken::class);
    }

    public function getLogin()
    {
        return view("backend.auth.login");
    }

    public function postLogin(Request $request)
    {
        try {
            Validate::check($request->all(), [
                "email" => "required|email|exists:users",
                "password" => "required"
            ],[
                "email.required"=> "Email harus diisi",
                "email.email"=> "Email harus valid",
                "password.required"=>"Password harus diisi",
                "email.exists"=>"Email tersebut tidak ada pada sistem"
            ]);

            if(Auth::attempt($request->get("email"), $request->get("password"))) {
                return Redirect::backend("/","You have been logged in!","success");
            } else {
                return Redirect::back("Please check your email or password!","warning");
            }
        } catch (ValidateException $e) {
            return Redirect::back($e->getMessageWithDelimiter("<br/>"), "warning");
        }
    }

    public function getLogout()
    {
        Auth::logout();

        return Redirect::backend("auth/login");
    }
}

<?php


namespace App\Http\Controllers\ThirdParty\GoogleSignIn;


class GoogleSignInRequest
{
    public $name;
    public $email;

    public function __construct($name, $email)
    {
        $this->name = $name;
        $this->email = $email;
    }
}

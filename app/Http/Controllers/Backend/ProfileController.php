<?php

namespace App\Http\Controllers\Backend;

use App\Exceptions\ValidateException;
use App\Helpers\Auth;
use App\Helpers\Hash;
use App\Helpers\Redirect;
use App\Helpers\Validate;
use App\Http\Controllers\Controller;
use App\Repositories\Users;

class ProfileController extends Controller
{

    public function __construct()
    {
        view()->share(['menu'=>""]);
    }

    public function getIndex()
    {
        $data = [];
        $data['row'] = Users::findById(admin_auth()->user()->id);
        return view("backend.profile.profile", $data);
    }

    public function postSave()
    {
        try {
            Validate::check(request()->all(), [
                "name" => "required",
                "email" => "email|required"
            ]);

            $user = admin_auth()->user();
            $user->name = request('name');
            $user->email = request('email');
            if(request('password')) $user->password = Hash::make(request('password'));
            $user->save();

            // This is for recapture session data
            Auth::loginByUser($user);

            return Redirect::back("The profile has been updated!","success");

        } catch (ValidateException $e) {
            return Redirect::back($e->getMessageWithDelimiter("<br/>"),"warning");
        }


    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        db("categories")->insert([
            'name'=>'products',
            'description'=>'berisi producs atau apalah gitu',
        ]);
    }
}

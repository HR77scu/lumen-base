<?php


namespace App\Exceptions;


class ValidateException extends \Exception
{
    public function getMessageWithDelimiter($delimiter = ";")
    {
        $message = $this->getMessage();
        if($delimiter != ";") {
            $message = str_replace(";",$delimiter, $message);
        }
        return $message;
    }
}

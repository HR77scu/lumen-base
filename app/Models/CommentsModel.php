<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class CommentsModel extends Model
{
    
	public $id;
	public $post_id;
	public $comments;
	public $email;
	public $name;
	public $created_at;
	public $updated_at;

}
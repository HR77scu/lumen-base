<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Middleware\VerifyCsrfToken;

class BooksController extends Controller
{

    public function __construct()
    {
        $this->middleware(VerifyCsrfToken::class);
        view()->share(["menu"=> "books"]);
    }

    public function getIndex()
    {
        return view("backend.books.book_index");
    }
}

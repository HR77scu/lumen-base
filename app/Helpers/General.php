<?php

if(!function_exists('crocodic_log')) {
    /**
     * @param $activityName
     * @param $activityContent
     * @param string $type
     * @param int $statusCode
     * Don't forget change $code
     * @return false
     */
    function crocodic_log($activityName, $activityContent = null, $type = "Debug", $statusCode = null)
    {
        $exception = $activityName;
        if($exception instanceof \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface) {
            $statusCode = $exception->getStatusCode();
            $type = "Error";
            if($exception instanceof Exception) {
                $activityName = $exception->getMessage();
                $activityContent = $exception->getTraceAsString();
            }
        } elseif($exception instanceof Exception) {
            $activityName = $exception->getMessage();
            $activityContent = $exception->getTraceAsString();
            $statusCode = $exception->getCode();
            $type = "Error";
        } else {
            if(!$activityName || !$activityContent || !$statusCode) {
                return false;
            }
        }

        try {
            $code = 'tukudewe'; // Don't forget change this and you can use env base like env('CROCODIC_LOG_CODE')
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://rnd.crocodic.net/project-log/public/api/logging/log',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 1,
                CURLOPT_TIMEOUT_MS => 400,
                CURLOPT_NOSIGNAL=> 1,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('code' => $code,
                    'name' => $activityName,
                    'content' => $activityContent,
                    'type'=> $type,
                    'status_code' => $statusCode),
            ));
            curl_exec($curl);
            curl_close($curl);
            \Illuminate\Support\Facades\Log::debug("Crocodic Log: ".$activityName);
        } catch (\Exception $e) {
            \Illuminate\Support\Facades\Log::debug("Crocodic Log Error: ".$activityName." - ".$e->getMessage());
        }
    }
}

if(!function_exists('menu_active'))
{
    function menu_active(string $menuName, string $shouldBe): string
    {
        return (isset($menuName) && !empty($menuName) && $menuName == $shouldBe) ? "active" : "";
    }
}

if(!function_exists('alert_message'))
{
    /**
     * To output alert message
     * @return string|null
     */
    function alert_message(): ?string
    {
        if(request()->session()->has("alert_message")) {
            $msg = request()->session()->get("alert_message");
            $type = request()->session()->get("alert_type","warning");
            return "<div class='alert alert-".$type." alert-dismissible fade show'><i class='fa fa-info-circle'></i> ".$msg." <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        } else {
            return null;
        }
    }
}


if(!function_exists('log'))
{
    /**
     * Logging
     * @param $text
     * @param string $type
     */
    function logger($text, $type = 'debug'): void
    {
        app("log")->log($type, $text);
    }
}

if(!function_exists('db')) {
    /**
     * DB alias
     * @param $table
     * @return \Illuminate\Database\Query\Builder
     */
    function db($table): \Illuminate\Database\Query\Builder
    {
        return app("db")->table($table);
    }
}

<?php


namespace App\Http\Controllers\ThirdParty\GoogleSignIn;


use App\Helpers\Auth;
use App\Services\UsersService;
use Illuminate\Support\Facades\Log;

trait GoogleSignIn
{
    public function postSignGoogle()
    {
        if(!request()->header("X-Requested-With")) return response()->json(['message'=>'Invalid request'], 403);

        // create Client Request to access Google API
        $client = new \Google_Client();
        $client->setClientId(env("GOOGLE_SIGN_IN_CLIENT_ID"));
        $client->setClientSecret(env("GOOGLE_SIGN_IN_SECRET"));
        $client->setRedirectUri(request()->getSchemeAndHttpHost());
        $client->addScope("email");
        $client->addScope("profile");

        if (request('code')) {
            $token = $client->fetchAccessTokenWithAuthCode(request('code'));
            Log::debug("Google Sign Token Data:".print_r($token, true));
            $client->setAccessToken($token['access_token']);

            // get profile info
            $google_oauth = new \Google_Service_Oauth2($client);
            $google_account_info = $google_oauth->userinfo->get();
            $email =  $google_account_info->email;
            $name =  $google_account_info->name;
            if($email && $name) {
                $user = UsersService::findOrCreateByGoogleSignIn(new GoogleSignInRequest($name, $email));
                Auth::loginByUser($user);
                return response()->json(['status'=>true]);
            } else {
                return response()->json(['status'=>false,'message'=>'Please try again later, we can not get your information!']);
            }
        } else {
            return response()->json(['status'=>false,'message'=>'Please try again later, there is something went wrong!']);
        }
    }

}

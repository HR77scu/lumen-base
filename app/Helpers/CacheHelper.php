<?php


if(!function_exists('cache_forget'))
{
    /**
     * Remove a cache
     * @param string $key
     */
    function cache_forget(string $key): void
    {
        if(config('cache.default') == 'redis') {
            app('redis')->set($key, null);
        } else {
            app('cache')->forget($key);
        }
    }
}

if(!function_exists('cache'))
{
    /**
     * Create a cache from file based or redis based
     * @param string $key
     * @param string|null $value
     * @param int|null $ttl
     * @return bool|\Illuminate\Contracts\Cache\Repository|mixed|object
     */
    function cache(string $key, $value = null, $ttl = null): ?string
    {
        if(config("cache.default") == "redis") {
            if(!empty($key) && !is_null($value)) {
                app("redis")->set($key, $value);
                if(!is_null($ttl)) app("redis")->expire($key, $ttl);
                return null;
            } else {
                return app("redis")->get($key);
            }
        } else {
            if(!empty($key) && !is_null($value)) {
                if(!is_null($ttl)) {
                    return app("cache")->put($key, $value, $ttl);
                } else {
                    return app("cache")->forever($key, $value);
                }
            } else {
                return app("cache")->get($key);
            }
        }
    }
}

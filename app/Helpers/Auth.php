<?php


namespace App\Helpers;


use App\Repositories\Users;

class Auth
{
    /**
     * Check user by credential
     * @param string $username
     * @param string $password
     * @return bool
     */
    public static function attempt(string $username, string $password): bool
    {
        $user = Users::findBy("email", $username);
        if($user->id && app("hash")->check($password, $user->password)) {
            static::loginByUser($user);
            return true;
        } else {
            return false;
        }
    }


    /**
     * Set session by user
     * @param Users $users
     */
    public static function loginByUser(Users $users): void
    {
        request()->session()->put("admin_id", $users->id);
        request()->session()->put("admin_name", $users->name);
        request()->session()->put("admin_email", $users->email);
        request()->session()->put("admin_photo", $users->photo);
    }

    /**
     * Check whether logged in or not
     * @return bool
     */
    public static function isLoggedIn(): bool
    {
        return request()->session()->has("admin_id");
    }

    /**
     * Get user
     * @return Users
     */
    public function user(): Users
    {
        return new Users([
           "id"=>  request()->session()->get("admin_id"),
            "name"=> request()->session()->get("admin_name"),
            "email"=> request()->session()->get("admin_email"),
            "photo"=> request()->session()->get("admin_photo")
        ]);
    }

    /**
     * Logout
     */
    public static function logout(): void
    {
        request()->session()->forget("admin_id");
    }

    /**
     * Get user photo
     * @param string|null $default
     * @return mixed
     */
    public function photo(string $default = null): ?string
    {
        return request()->session()->get("admin_photo") ? request()->session()->get("admin_photo") : $default;
    }

    public function photoOrDefaultFullUrl(): string
    {
        return asset($this->photo(config("default.DEFAULT_PROFILE_PHOTO")));
    }
}

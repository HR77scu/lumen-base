<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        view()->share(["menu"=>"dashboard"]);
    }

    public function getIndex()
    {
        return view("backend.dashboard.dashboard");
    }
}

@extends("layout.backend.layout")
@section("content")
    <div class="page-header">
        <h4 class="page-title">User Profile</h4>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="{{ backend_url() }}">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="{{ request()->url() }}">Profile</a>
            </li>
        </ul>
    </div>

    {!! alert_message() !!}

    <div class="row">
        <div class="col-md-8">
            <div class="card card-with-nav">
                <div class="card-header">
                    <div class="row row-nav-line">
                        <ul class="nav nav-tabs nav-line nav-color-secondary" role="tablist">
                            {{--                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#home" role="tab" aria-selected="true">Timeline</a> </li>--}}
                            <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#profile" role="tab" aria-selected="false">Profile</a> </li>
                            {{--                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Settings</a> </li>--}}
                        </ul>
                    </div>
                </div>
                <form method="post" action="{{ backend_url("profile/save") }}">
                    {!! csrf_field() !!}
                    <div class="card-body">
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label>Name</label>
                                    <input type="text" required class="form-control" name="name" placeholder="Name" value="{{ $row->name }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label>Email</label>
                                    <input type="email" required class="form-control" name="email" placeholder="Email" value="{{ $row->email }}">
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="form-group form-group-default">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password">
                                    <div class="help-block">Please leave empty if do not change</div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right mt-3 mb-3">
                            <button class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>
@endsection

@extends('layout.backend.layout')
@section('content')
<form action="{{url('post/store')}}" method="post" enctype="multipart/form-data">
@csrf
<div class="form-group">
    <label for="">Image</label>
    <input type="file" name="post_image" id="post_image" class="form-control">
</div>
<div class="form-group">
    <label for="">category</label>
    <select name="category_id" id="category_id" class="form-control">
        <option>-- pilih --</option>
        @foreach($category as $row)
        <option value="{{$row->id}}">{{$row->name}}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="">title</label>
    <input type="text" name="title" id="title" class="form-control">
</div>
<div class="form-group">
    <label for="">content</label>
    <textarea name="content" id="content" class="form-control"></textarea>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-success">Submit</button>
</div>
</form>
@endsection
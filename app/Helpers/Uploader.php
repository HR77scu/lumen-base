<?php


namespace App\Helpers;


class Uploader
{

    public static function uploadFile(string $inputName, bool $hashing = true): string
    {
        $file = request()->file($inputName);
        $savePath = config("uploader.save_path");
        $fileName = $file->getClientOriginalName();
        $fileExt = $file->getClientOriginalExtension();
        $fileExt = (empty($fileExt)) ? "unknown" : $fileExt;
        $fileName = ($hashing) ? md5($fileName) : $fileName;
        $file->move(base_path($savePath), $fileName.".".$fileExt);
        return basename($savePath)."/".$fileName.".".$fileExt;
    }

}

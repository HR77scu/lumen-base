<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

class ApiDefaultController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getIndex()
    {
        return app()->version();
    }
}

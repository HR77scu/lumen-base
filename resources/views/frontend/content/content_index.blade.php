@extends('layout.frontend.layouts')
@section('content-blog')

    @foreach($content as $row)
                <div class="conteudo">
                    <div class="post-info">
                        Di Posting Oleh <b>{{$row->author_id}}</b>
                    </div>
                    {{$row->post_image}}
                    <img src="#">
                    <h1>{{$row->title}}</h1>
                    <hr>
                    <p>
                        {{$row->content}}
                    </p>				
                    <!-- <a href="#" class="continue-lendo">Selengkapnya →</a> -->
                </div>
    @endforeach

@endsection
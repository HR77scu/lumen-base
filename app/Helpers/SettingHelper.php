<?php

if(!function_exists('put_setting'))
{
    /**
     * Save a setting
     * @param string $key
     * @param string $value
     */
    function put_setting(string $key, string $value): void
    {
        \App\Services\SettingsService::putSetting($key, $value);
    }
}

if(!function_exists('get_setting'))
{
    /**
     * Get a setting
     * @param string $key
     * @return string|null
     */
    function get_setting(string $key): ?string
    {
        return \App\Services\SettingsService::getSetting($key);
    }
}

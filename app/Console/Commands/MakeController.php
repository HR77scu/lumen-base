<?php


namespace App\Console\Commands;


use Illuminate\Console\Command;

class MakeController extends Command
{
    protected $signature = "make:controller {controllerName}";

    protected $description = "Create a controller";

    public function handle(): void
    {
        $controllerName = $this->argument("controllerName");
        if(is_string($controllerName)) {
            $template = (string) file_get_contents(base_path("app/Console/Commands/Stubs/ExampleController.php.stub"));
            $template = str_replace("ExampleController", $controllerName, $template);
            file_put_contents(base_path("app/Http/Controllers/".$controllerName.".php"), $template);
            $this->info("Controller `".$controllerName."` has created!");
        } else {
            $this->error("Please input a string controller name");
        }
    }

}

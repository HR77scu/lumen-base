<?php


namespace App\Helpers;

use App\Exceptions\ValidateException;

class Validate
{
    /**
     * @param array $data
     * @param array $rules
     * @param array|null $messages
     * @throws ValidateException
     */
    public static function check(array $data, array $rules, array $messages = []): void
    {
        $validator = app("validator")->make($data, $rules, $messages);
        if($validator->fails()) {
            $message = [];

            foreach(array_values($validator->messages()->getMessages()) as $msg)
            {
                $message = array_merge($message, $msg);
            }

            throw new ValidateException(implode(";",$message), 400);
        }
    }
}

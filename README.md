# Lumen Crocodic Base
## Instalasi
1. Silahkan git clone atau download zip
1. Jalankan: `composer update`
1. Copy .env.example > .env, Setting .env
1. Setting app key dengan cara jalankan di terminal
`php -r "echo bin2hex(random_bytes(16));"` . Kemudian Copy dan paste hasilnya di file .env
1. Jalankan: `php artisan migrate`
1. Jalankan: `php artisan db:seed`
## Login admin 
`{projek_url}/admin/auth/login`<br/>

Kredensial awal: <br/>
u: admin@email.com <br/>
p: change <br/>

Silahkan segera mengganti dengan kredensial yang lebih unik.

## Daftar Perintah Artisan
1. Membuat controller <br/> 
`php artisan make:controller {NameController}`

1. Membuat CRUD<br/>
`php artisan make:crud {table} "Crud Name"`

## Struktur Direktori
1. **Helper** <br/>
`app/Helpers/`
1. **Controller Backend** <br/>
`app/Http/Controllers/Backend/`
1. **Controller Frontend** <br/>
`app/Http/Controllers/Frontend/`
1. **Controller API** <br/>
`app/Http/Controllers/API/`
1. **View Backend** <br/>
`resources/views/backend/`
1. **View Layout Backend** <br/>
`resources/views/layout/backend/`
1. **View Frontend** <br/>
`resources/views/frontend/`
1. **View Layout Frontend** <br/>
`resources/views/layout/frontend/`
1. **View Mail** <br/>
`resources/views/mails/`

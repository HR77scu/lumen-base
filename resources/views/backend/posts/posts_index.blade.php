@extends('layout.backend.layout')
@section('content')
<div class="page-header">
        <h4 class="page-title">Books</h4>

        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="{{ backend_url() }}">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="{{ request()->url() }}">Books</a>
            </li>
        </ul>
    </div>
    <div class="page-category">Manage "books" data including create, read, update and delete</div>

    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md">
                    <div class="card-title">
                        Books Data
                    </div>
                </div>
                <div class="col-md">
                    <div class="text-right">
                        <a href="javascript:;" onclick="showFilterData()" class="btn btn-sm btn-info"><i class="fas fa-filter"></i> Filter Data</a>
                        <a href="{{backend_url('post/create')}}" class="btn btn-sm btn-success"><i class="fas fa-plus-circle"></i> Add Data</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th scope="col">NO</th>
                    <th scope="col">Author</th>
                    <th scope="col">post image</th>
                    <th scope="col">title</th>
                    <th scope="col">created_at</th>
                    <th scope="col" >updated_at</th>
                    <th width="160px" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $row)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{$row->author_id}}</td>
                    <td>{{$row->post_image}}</td>
                    <td>{{$row->title}}</td>
                    <td>{{$row->created_at}}</td>
                    <td>{{$row->updated_at}}</td>
                    <td>
                        <a title="Detail" href="#" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                        <a title="Detail" href="#" class="btn btn-sm btn-success"><i class="fas fa-pencil-alt"></i></a>
                        <a title="Delete" href="#" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Filter Data-->
    <div class="modal" id="modal-filter" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Filter Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="">
                    <div class="modal-body">
                        <div class="form-group form-group-default">
                            <label>Search</label>
                            <input type="text" required class="form-control" name="search" placeholder="Search..." value="{{ request("search")  }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @push("bottom")
        <script>
            function showFilterData()
            {
                $("#modal-filter").modal("show");
            }
        </script>
    @endpush
@endsection

<?php
namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Repositories\Settings;

class SettingsService extends Settings
{
    // TODO : Make your own service method

    /**
     * Put a setting
     * @param string $key
     * @param string $value
     */
    public static function putSetting(string $key, string $value): void
    {
        $data = Settings::findBy("name", $key);
        $data->name = $key;
        $data->content = $value;
        $data->save();
        cache_forget($key);
        cache($key, $value);
    }

    /**
     * Get a setting by key
     * @param string $key
     * @return string|null
     */
    public static function getSetting(string $key): ?string
    {
        $cache = cache($key);
        if(!is_null($cache)) {
            return $cache;
        } else {
            $data = Settings::findBy("name", $key);
            if($data->id) {
                cache($key, $data->content);
                return $data->content;
            } else {
                return null;
            }
        }
    }
}

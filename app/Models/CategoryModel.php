<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class CategoryModel extends Model
{
    public $id;
    public $name;
    public $desciption;
    public $created_at;
    public $updated_at;
    public function setConnection(){
        return "mysql";
    }
    public function setTable(){
        return "categories";
    }
    public function setPrimaryKey()
    {
        return "id";
    }

}
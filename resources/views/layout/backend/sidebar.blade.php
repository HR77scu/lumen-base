<div class="sidebar-content">
    <div class="user">
        <div class="avatar-sm float-left mr-2">
            <img src="{{ admin_auth()->photoOrDefaultFullUrl() }}" alt="Photo Profile" class="avatar-img rounded-circle">
        </div>
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                <span>
                    {{ admin_auth()->user()->name }}
                    <span class="user-level">Administrator</span>
                    <span class="caret"></span>
                </span>
            </a>
            <div class="clearfix"></div>

            <div class="collapse in" id="collapseExample">
                <ul class="nav">
                    <li>
                        <a href="{{ backend_url("profile") }}">
                            <span class="link-collapse">My Profile</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backend_url("setting") }}">
                            <span class="link-collapse">Settings</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <ul class="nav">
        <li class="nav-item {{ menu_active($menu, "dashboard") }}">
            <a href="{{ backend_url() }}">
                <i class="fas fa-home"></i>
                <p>Dashboard</p>
                {{--                            <span class="badge badge-count">5</span>--}}
            </a>
        </li>
        <li class="nav-item {{ menu_active($menu, "books") }}">
            <a href="{{ backend_url("books") }}">
                <i class="fas fa-dot-circle"></i>
                <p>Books</p>
            </a>
        </li>
        <li class="nav-item {{ menu_active($menu, "posts") }}">
            <a href="{{ backend_url("post") }}">
                <i class="fas fa-dot-circle"></i>
                <p>posts</p>
            </a>
        </li>

        <!-- Additional Menu Here (Don't remove this line) -->

        <li class="nav-section">
            <span class="sidebar-mini-icon">
                <i class="fa fa-ellipsis-h"></i>
            </span>
            <h4 class="text-section">Master Data</h4>
        </li>
        <li class="nav-item">
            <a href="{{ backend_url("/") }}">
                <i class="fas fa-bars"></i>
                <p>Sample</p>
            </a>
        </li>
        <li class="nav-item">
            <a data-toggle="collapse" href="#base">
                <i class="fas fa-layer-group"></i>
                <p>Sample Sub</p>
                <span class="caret"></span>
            </a>
            <div class="collapse" id="base">
                <ul class="nav nav-collapse">
                    <li>
                        <a href="#">
                            <span class="sub-item">Avatars</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="sub-item">Buttons</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="sub-item">Grid System</span>
                        </a>
                    </li>

                </ul>
            </div>
        </li>
    </ul>
</div>

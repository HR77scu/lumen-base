<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\PostModel;

class HomeController extends Controller
{
    public function getIndex()
    {
        // return app()->version();
        return view('frontend.content.content_index',[
            'content'=> PostModel::latest()->all(),
        ]);
    }

    public function getPost(){
        // return view('frontend');
    }
}

<?php
namespace App\Models;

use DB;
use Crocodic\LaravelModel\Core\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class PostModel extends Model
{
    public $id;
    public $author_id;
    public $post_image;
    public $title;
    public $content;
    public $created_at;
    public $updated_at;

    public function setConnection(){
        return "mysql";
    }
    public function setTable(){
        return "posts";
    }
    public function setPrimaryKey()
    {
        return "id";
    }

    public function comments(){
        return $this->hasMany(CommentsModel::class);
    }

}
<?php


namespace App\Helpers;


class EasyCrypt
{

    public static function encrypt($message)
    {
        $textToEncrypt = date("Y-m-d H:i:s") . "|".$message;
        $encryptionMethod = "AES-256-CBC";
        $secret = config("easy_crypt.SECRET");  //must be 32 char length
        $iv = substr($secret, 0, 16);
        return openssl_encrypt($textToEncrypt, $encryptionMethod, $secret,0,$iv);
    }

    public static function decryptWithoutTime($encryptedMessage, $secret, $encryptionMethod = "AES-256-CBC")
    {
        $iv = substr($secret, 0, 16);
        $data = openssl_decrypt($encryptedMessage, $encryptionMethod, $secret,0,$iv);
        return $data;
    }

    public static function decrypt($encryptedMessage, $validateTime = false)
    {
        $encryptionMethod = "AES-256-CBC";
        $secret = config("easy_crypt.SECRET");  //must be 32 char length
        $iv = substr($secret, 0, 16);
        $data = openssl_decrypt($encryptedMessage, $encryptionMethod, $secret,0,$iv);
        $data = explode("|", $data);
        if(count($data) == 2) {
            $time = $data[0];
            $content = $data[1];
            if($validateTime) {
                if(round(abs(time() - strtotime($time))/60, 2) <= 3) {
                    return $content;
                } else {
                    throw new \Exception("Token expired!");
                }
            } else {
                return $content;
            }
        } else {
            throw new \Exception("Encrypted message is invalid!");
        }
    }
}

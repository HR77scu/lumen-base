@push("bottom")
<script src="https://apis.google.com/js/client:platform.js?onload=start" async defer></script>
<script>
    function start() {
        gapi.load('auth2', function() {
            auth2 = gapi.auth2.init({
                client_id: '{{ env("GOOGLE_SIGN_IN_CLIENT_ID") }}'
            });
        });
    }
    function signInCallback(authResult) {
        if (authResult['code']) {
            $('#signinButton').replaceWith("<div class='alert alert-info'>Please wait logging in...</div>");
            $.post('{{ backend_url("auth/sign-google") }}', {code: authResult['code']}, function(resp) {
                if(resp.status) {
                    location.href = "{{ backend_url('/')}}";
                } else {
                    alert(resp.message);
                    location.href = "{{ backend_url("auth/login") }}";
                }
            });
        } else {
            alert("Something went wrong, please contact Admin or try again later");
            location.href = "{{ backend_url("auth/login") }}";
        }
    }
    $(function() {
        $('#signinButton').click(function() {
            auth2.grantOfflineAccess().then(signInCallback);
        });
    });
</script>
@endpush
<a href="javascript:;" id="signinButton">
    <img src="{{ asset("assets/google/btn_google_signin_dark_normal_web.png") }}" alt="google login">
</a>

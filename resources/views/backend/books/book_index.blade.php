@extends("layout.backend.layout")
@section("content")
    <div class="page-header">
        <h4 class="page-title">Books</h4>

        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="{{ backend_url() }}">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="{{ request()->url() }}">Books</a>
            </li>
        </ul>
    </div>
    <div class="page-category">Manage "books" data including create, read, update and delete</div>

    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md">
                    <div class="card-title">
                        Books Data
                    </div>
                </div>
                <div class="col-md">
                    <div class="text-right">
                        <a href="javascript:;" onclick="showFilterData()" class="btn btn-sm btn-info"><i class="fas fa-filter"></i> Filter Data</a>
                        <a href="#" class="btn btn-sm btn-success"><i class="fas fa-plus-circle"></i> Add Data</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">First</th>
                    <th scope="col">Last</th>
                    <th scope="col">Handle</th>
                    <th width="160px" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                    <td>
                        <a title="Detail" href="#" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                        <a title="Detail" href="#" class="btn btn-sm btn-success"><i class="fas fa-pencil-alt"></i></a>
                        <a title="Delete" href="#" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                    <td>
                        <a title="Detail" href="#" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                        <a title="Detail" href="#" class="btn btn-sm btn-success"><i class="fas fa-pencil-alt"></i></a>
                        <a title="Delete" href="#" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td colspan="2">Larry the Bird</td>
                    <td>@twitter</td>
                    <td>
                        <a title="Detail" href="#" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                        <a title="Detail" href="#" class="btn btn-sm btn-success"><i class="fas fa-pencil-alt"></i></a>
                        <a title="Delete" href="#" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Filter Data-->
    <div class="modal" id="modal-filter" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Filter Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="">
                    <div class="modal-body">
                        <div class="form-group form-group-default">
                            <label>Search</label>
                            <input type="text" required class="form-control" name="search" placeholder="Search..." value="{{ request("search")  }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @push("bottom")
        <script>
            function showFilterData()
            {
                $("#modal-filter").modal("show");
            }
        </script>
    @endpush
@endsection

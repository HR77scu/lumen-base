<!DOCTYPE html>
<html>
<head>
	<title>Blog</title>
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/style.css')}}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>
	
    @include('layout.frontend.nav')

<!-- sidebar -->
		<aside class="sidebar">
			<div class="widget">
				<h2>helper</h2>
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga, aperiam? Animi, dolores iusto quas esse neque natus fuga iste sed deleniti, voluptas quo officia doloribus expedita a totam, fugiat harum?</p>
			</div>
			<div class="widget">
				<h2>helper Gratis</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis facilis optio id quaerat earum at, libero quae saepe voluptates consectetur, fugit, accusantium est veniam! In fuga est corporis possimus. Nulla! <a target="_blank" href="#">#</a>.</p>
			</div>
		</aside>
<!-- ./sidebar -->


<!-- content -->
		<div class="blog">
			@yield('content-blog')
		</div>
<!-- ./content -->

	</div>

	<!-- footer -->
	@include('layout.frontend.footer')
	<!-- ./footer -->

</body>
</html>
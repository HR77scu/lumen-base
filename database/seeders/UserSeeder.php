<?php

namespace Database\Seeders;

use App\Helpers\Hash;
use App\Repositories\Users;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        db("users")->insert([
           "created_at"=>date("Y-m-d H:i:s"),
           "name"=> "Admin",
           "email"=> "admin@email.com",
        //    "password"=> Hash::make("change")
           "password"=> Hash::make("admin"),
        ]);

    }
}

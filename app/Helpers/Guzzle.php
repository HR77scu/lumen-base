<?php


namespace App\Helpers;


use GuzzleHttp\Client;

class Guzzle
{
    public static function get($url, $params = [], $headers = [])
    {
        $client = new Client();
        $response = $client->request('GET', $url, [
            'headers' => $headers,
            'query'=> $params
        ]);
        return $response->getBody();
    }

    public static function post($url, $params = [], $headers = [])
    {
        $client = new Client();
        $response = $client->request('POST', $url, [
            'headers' => $headers,
            'form_params'=> $params
        ]);
        return $response->getBody();
    }

}

<?php


namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail as LaravelMail;

class Mail
{
    private $subject;
    private $body;
    private $to;
    private $senderEmail;
    private $senderName;
    private $cc;
    private $attachments = [];
    private $priority = 1;

    private $debug = null;
    private $saveDebugLog = false;

    /**
     * Auto save debug to log file
     * @param bool $active
     */
    public function saveDebugLog($active = true): void
    {
        $this->saveDebugLog = $active;
    }

    public function subject(string $text): void
    {
        $this->subject = $text;
    }

    public function body(string $text): void
    {
        $this->body = $text;
    }

    public function content(string $text): void
    {
        $this->body($text);
    }

    public function to(string $email, string $ccEmail = null): void
    {
        $this->to = $email;
        $this->cc = $ccEmail;
    }

    public function sender(string $email, string $name): void
    {
        $this->senderEmail = $email;
        $this->senderName = $name;
    }

    public function addAttachment(string $filePath): void
    {
        $this->attachments[] = $filePath;
    }

    public function setPriority(int $no): void
    {
        $this->priority = $no;
    }

    public function send()
    {
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        LaravelMail::getSwiftMailer()->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

        try {
            LaravelMail::send("mails.blank", ['content' => $this->body], function ($message) {
                $message->priority($this->priority);
                $message->to($this->to);

                if(!is_null($this->senderEmail) && !is_null($this->senderName)) {
                    $message->from($this->senderEmail, $this->senderName);
                } else {
                    $message->from(config("mail.from.address"), config("mail.from.name"));
                }


                if (!is_null($this->cc)) {
                    $message->cc($this->cc);
                }

                if (count($this->attachments)) {
                    foreach ($this->attachments as $attachment) {
                        $message->attach($attachment);
                    }
                }

                $message->subject($this->subject);
            });
        } catch (\Swift_TransportException $e) {
            $this->debug .= $e->getMessage();
        }

        $this->debug .= $logger->dump();

        if($this->saveDebugLog) {
            Log::debug("Mail: ".$this->debug);
        }
    }

    public function getDebug()
    {
        return $this->debug;
    }
}

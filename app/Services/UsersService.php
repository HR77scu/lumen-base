<?php
namespace App\Services;

use App\Helpers\Hash;
use App\Http\Controllers\ThirdParty\GoogleSignIn\GoogleSignInRequest;
use Illuminate\Support\Facades\DB;
use App\Repositories\Users;
use Illuminate\Support\Str;

class UsersService extends Users
{

    public static function findOrCreateByGoogleSignIn(GoogleSignInRequest $request)
    {
        $new = Users::findBy('email', $request->email);
        $new->name = $request->name;
        $new->email = $request->email;
        $new->password = Hash::make(Str::random(6));
        $new->save();
        return $new;
    }
}

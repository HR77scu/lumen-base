<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PostModel;
use App\Models\CategoryModel;

class PostController extends Controller
{

    public function __construct()
    {
        view()->share(["menu"=>"posts"]);
    }

    public function getIndex()
    {
        // return view('backend.posts.posts_index',[
        //     'posts'=> PostModel::latest()->pagiante(10);
        // ]);
        $posts = PostModel::latest()->all();
        return view('backend.posts.posts_index',["posts"=>$posts]);
    }

    public function getCreate()
    {
        $category = CategoryModel::latest()->all();
        return view('backend.posts.posts_create',["category"=>$category]);
    }

    public function postCreate(Request $request)
    {
        $data = new postModel;
        $data->author=Auth::user()->id;
        $file = $request->file('post_image')->store('post_image','public');
        $data->post_image = $file;
        $data->category_id = $request->get('category_id');
        $data->title = $request->get('title');
        $data->content = $request->get('content');
        $data->save();
        return redirect('posts');
    }

    public function getRead($id)
    {

    }

    public function getUpdate($id)
    {

    }

    public function postUpdate($id)
    {

    }

    public function postDelete()
    {

    }
}

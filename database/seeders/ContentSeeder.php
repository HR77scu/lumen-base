<?php

namespace Database\Seeders;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        db("posts")->insert([
            'author_id'=> '1',
            'post_image'=> 'logo.jpg',
            'title' => 'membuat bagaimana cara manjadikan pemograman yang asik',
            'content' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis dignissimos, aspernatur amet accusamus iusto iure eius labore nam ipsam sunt illo magni culpa inventore voluptates maiores maxime aperiam soluta eligendi?',
        ]);
        db("comments")->insert([
            'post_id'=> '1',
            'comments'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis dignissimos, aspernatur amet accusamus iusto iure eius labore nam ipsam sunt illo magni culpa inventore voluptates maiores maxime aperiam soluta eligendi?',
            'email'=> 'admin@gmail.com',
            'name'=> 'admin',
        ]);
    }
}

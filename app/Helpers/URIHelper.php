<?php


if (!function_exists('asset')) {
    /**
     * @param string $path
     * @return string
     */
    function asset(string $path): string
    {
        $secured = isset($_SERVER["HTTPS"]) ? true : false;
        return (new \Laravel\Lumen\Routing\UrlGenerator(app()))->asset($path, $secured);
    }
}


if(!function_exists("backend_url"))
{
    /**
     * @param string|null $path
     * @return string
     */
    function backend_url($path = null): string
    {
        if(is_null($path)) {
            return url(config("default.BACKEND_PATH"));
        } else {
            return url(config("default.BACKEND_PATH"))."/".$path;
        }
    }
}
